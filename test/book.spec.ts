import 'mocha'
import * as Request from 'supertest'
export const bookData = {
  firstName: 'test',
  lastName: 'test lastName',
  authorId: 1
}
export const apiKey = '98911acd-1b58-415c-988a-721b64898a40'
export const request = Request('http://localhost:3000')
export const bookUpdatedId = 1
export const bookDeletedId = 25
describe('Book Crud ',  () => {
  it('Book create', async () => {
    const result = await request.post('/api/books')
      .send(bookData)
      .set('apikey', apiKey)
      .expect(201)
  })

  it('Book get', async () => {
    const result = await request.get('/api/books')
      .set('apikey', apiKey)
      .expect(200)
  })
  it('Book patch', async () => {
    const result = await request.patch(`/api/authors?id=${bookUpdatedId}`)
      .send(bookData)
      .set('apikey', apiKey)
      .expect(200)
  })
  it('Book delete', async () => {
    const result = await request.delete(`/api/authors?id=${bookDeletedId}`)
      .set('apikey', apiKey)
      .expect(202)
  })
})
