import { expect } from 'chai'
import 'mocha'
import * as express from 'express'
import * as Request from 'supertest'
export  const app = express()
export const AuthorData = {
  firstName: 'test',
  lastName: 'test lastName'
}
export const authorUpdatedId = 1
export const bookDeletedId = 25
export const request = Request('http://localhost:3000')
export const apiKey = '98911acd-1b58-415c-988a-721b64898a40'

describe('Author Crud ',  () => {
  it('Author create', async () => {
    const result = await request.post('/api/authors')
      .send(AuthorData)
      .set('apikey', apiKey)
      .expect(201)
  })

  it('Author get', async () => {
    const result = await request.get('/api/authors')
      .set('apikey', apiKey)
      .expect(200)
  })

  it('Author patch', async () => {
    const result = await request.patch(`/api/authors?id=${authorUpdatedId}`)
      .send(AuthorData)
      .set('apikey', apiKey)
      .expect(200)
  })
  it('Author delete', async () => {
    const result = await request.delete(`/api/authors?id=${bookDeletedId}`)
      .set('apikey', apiKey)
      .expect(202)
  })
})
