
# Introduction

 Typescript project with NodeJS + Express + Sequelize ORM

## Installation

Run one of the command below

```bash
npm install
```
## migrate DB

update src/sqlz/config env
```bash
npm run sqlz:migrate
```
## seeder DB
```bash
npm run sqlz:seeder
```

## run test
```bash
npm run test
```
