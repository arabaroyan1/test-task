import * as AuthorsDao from './authors'
import * as BooksDao from './books'

export { AuthorsDao }
export { BooksDao }
