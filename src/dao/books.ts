import {Author, AuthorModel} from '../sqlz/models/author'
import { Book } from '../sqlz/models/book'
import {ErrorDto} from '../dto/errorDto'
import * as Sqlz from 'sequelize'

export async function create(data: any): Promise<Book | ErrorDto> {
  const authors: Author = await Author.findOne({
    where: { id: data.authorId }
  })
  if (!authors) {
    return {
      error: `Author for this id not found`
    }
  }
  return await Book
    .create({
      firstName: data.firstName,
      lastName: data.lastName,
      authorId: authors.get('id')
    })
}

export async function  update(id: number, bookData): Promise< boolean | ErrorDto > {
  const authors: Author = await Author.findOne({
    where: { id: bookData.authorId }
  })

  if (!authors) {
    return {
      error: `Author for this id not found`
    }
  }

  await Book
    .update(
      bookData,
      {
        where: { id }
      }
    )
  return true
}

export async function deleteBook(id: number): Promise< boolean > {
  await Book
    .destroy({where: {
        id
      }})
  return true
}

export async function findAll(limit: number, offset: number, firstName: string, lastName: string, authorId: number): Promise<Book[]> {
  let where = {}

  if (firstName) {
    where['firstName'] = {
      [Sqlz.Op.like]: `%${firstName}%`
    }
  }
  if (lastName) {
    where['lastName'] = {
      [Sqlz.Op.like]: `%${lastName}%`
    }
  }

  if (authorId) {
    where['authorId'] = authorId
  }

  return await Book
    .findAll( {
      include: [{
        model: Author,
        as: 'authors'
      }],
      where,
      offset,
      limit
    })
}

export async function getCount(firstName: string, lastName: string, authorId: number): Promise<number> {
  let where = {}

  if (firstName) {
    where['firstName'] = {
      [Sqlz.Op.like]: `%${firstName}%`
    }
  }
  if (lastName) {
    where['lastName'] = {
      [Sqlz.Op.like]: `%${lastName}%`
    }
  }

  if (authorId) {
    where['authorId'] = authorId
  }

  const dataCount =  await Book
    .findAll({
        attributes: [[Sqlz.fn('COUNT', Sqlz.col('id')), 'count']],
        where,
        raw: true,
      },
    )
  return  dataCount[0]['count']
}
