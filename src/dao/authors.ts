import {Author, AuthorModel} from '../sqlz/models/author'
import {Book} from '../sqlz/models/book'
import * as Sqlz from 'sequelize'

export function create(author: AuthorModel): Promise<Author> {
  return Author
    .create({
      firstName: author.firstName,
      lastName: author.lastName
    })
}

export async function update(id: number, authorData): Promise< boolean > {
  await Author
    .update(
      authorData,
      {
        where: { id }
      }
      )
  return true
}

export async function deleteAuthor(id: number): Promise< boolean > {
  await Author
    .destroy({where: {
      id
      }})
  return true
}

export async function findAll(limit: number, offset: number, firstName: string, lastName: string): Promise<Author[]> {
  let where = {}

  if (firstName) {
    where['firstName'] = {
      [Sqlz.Op.like]: `%${firstName}%`
      }
  }
  if (lastName) {
    where['lastName'] = {
      [Sqlz.Op.like]: `%${lastName}%`
    }
  }

  return await Author
    .findAll({
      include: [{
        model: Book,
        as: 'books'
      }],
        where,
        offset,
        limit
    },
    )
}

export async function getCount(firstName: string, lastName: string): Promise<number> {
  let where = {}

  if (firstName) {
    where['firstName'] = {
      [Sqlz.Op.like]: `%${firstName}%`
    }
  }
  if (lastName) {
    where['lastName'] = {
      [Sqlz.Op.like]: `%${lastName}%`
    }
  }

  const dataCount =  await Author
    .findAll({
      attributes: [[Sqlz.fn('COUNT', Sqlz.col('id')), 'count']],
        where,
        raw: true,
      },
    )
  return  dataCount[0]['count']
}
