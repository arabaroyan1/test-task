import { Express } from 'express'
import { BookController } from '../controllers/_index'
import {body} from 'express-validator'
import {apiKey} from '../middlewares/api.middleware'


export function routes(app: Express) {

  app.get('/api/books', apiKey, BookController.BooksGet.list)
  app.post('/api/books',
    apiKey,
    body('firstName').exists().isLength({min: 2 }),
    body('lastName').exists().isLength({min: 2 }),
    BookController.BooksPost.create)
  app.patch('/api/books',
    apiKey,
    body('firstName').exists().isLength({min: 2 }),
    body('lastName').exists().isLength({min: 2 }),
    BookController.BooksPatch.update)
  app.delete('/api/books', apiKey, BookController.BooksDelete.deleteBook)
}
