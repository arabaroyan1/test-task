import { Express } from 'express'
import { AuthorController } from '../controllers/_index'
import { body } from 'express-validator'
import {apiKey} from '../middlewares/api.middleware'

export function routes(app: Express) {

  app.get('/api/authors', apiKey, AuthorController.AuthorGet.list)
  app.post('/api/authors',
    apiKey,
    body('firstName').exists().isLength({min: 2 }),
    body('lastName').exists().isLength({min: 2 }),
    AuthorController.AuthorPost.create)
  app.patch('/api/authors',
    apiKey,
    body('firstName').exists().isLength({min: 2 }),
    body('lastName').exists().isLength({min: 2 }),
    AuthorController.AuthorPatch.update)
  app.delete('/api/authors', apiKey, AuthorController.AuthorDelete.deleteAuthor)
}
