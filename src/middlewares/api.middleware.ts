import express from 'express'
import { config } from '../constants'

export function apiKey(req: express.Request, res: express.Response, next: express.NextFunction) {

  if (!req.headers.apikey || req.headers.apikey !== config.apiKey) {

    return res.status(400).json({
      error: 'Bad Request',
      message: 'missing api key'
    })
  }

  next()
}
