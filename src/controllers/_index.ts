import * as AuthorController from './authors/_index'
import * as BookController from './books/_index'

export { BookController, AuthorController }
