import * as BooksGet from './books.get'
import * as BooksPost from './books.post'
import * as BooksPatch from './books.patch'
import * as BooksDelete from './books.delete'

export { BooksGet, BooksPost, BooksPatch, BooksDelete }
