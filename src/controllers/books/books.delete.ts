import { Request, Response } from 'express'
import {BooksDao} from '../../dao/_index'

export async function deleteBook(req: Request, res: Response) {
   try {
    if (!req.query.id) {
      return res.status(400).json({
        error: 'Bad Request',
        message: 'missing id'
      })
    }
    const books: any = await BooksDao.deleteBook(req.queryss.id)
    return  res.status(204).send(books)
  } catch (e) {
    res.boom.badRequest(e)
  }
}
