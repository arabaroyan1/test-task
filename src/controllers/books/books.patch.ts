import { Request, Response } from 'express'
import {AuthorsDao, BooksDao} from '../../dao/_index'
import {validationResult} from 'express-validator'

export async function update(req: Request, res: Response) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() })
  }

  try {
    if (!req.query.id) {
      return res.status(400).json({
        error: 'Bad Request',
        message: 'missing id'
      })
    }
    const books: any = await BooksDao.update(req.query.id, req.body)

    if (books !== true && 'error' in books) {
      return  res.status(422).send(books)
    }

    return  res.status(201).send(books)
  } catch (e) {
    res.boom.badRequest(e)
  }
}
