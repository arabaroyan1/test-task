import { Request, Response } from 'express'
import {AuthorsDao, BooksDao} from '../../dao/_index'
import {validationResult} from 'express-validator'
import {Book} from '../../sqlz/models/book'
import {ErrorDto} from '../../dto/errorDto'

export async function create(req: Request, res: Response) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() })
  }

  try {
    const books: Book | ErrorDto = await BooksDao.create(req.body)

    if ('error' in books) {
     return  res.status(422).send(books)
    }

    return  res.status(201).send(books)
  } catch (e) {
    res.boom.badRequest(e)
  }
}
