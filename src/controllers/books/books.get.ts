import { Request, Response } from 'express'
import { BooksDao} from '../../dao/_index'
import {config} from '../../constants'

export async function list(req: Request, res: Response) {
  const limit = config.limit
  const offset = req.query.page ? (req.query.page - 1) * limit : 0
  try {
    const books = await BooksDao.findAll(limit, offset, req.query.firstName, req.query.lastName, req.query.authorId)
    const count: number = await BooksDao.getCount(req.query.firstName, req.query.lastName, req.query.authorId)
    return  res.status(200).send({...books, ...{count}})
  } catch (e) {
    res.boom.badRequest(e)
  }
}
