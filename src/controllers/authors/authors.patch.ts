import { Request, Response } from 'express'
import { AuthorsDao } from '../../dao/_index'
import {validationResult} from 'express-validator'

export async function update(req: Request, res: Response) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() })
  }

  try {
    if (!req.query.id) {
      return res.status(400).json({
        error: 'Bad Request',
        message: 'missing id'
      })
    }

    const authors: any = await AuthorsDao.update(req.query.id, req.body)
    if (authors !== true && 'error' in authors) {
      return  res.status(422).send(authors)
    }
    return  res.status(200).send(authors)
  } catch (e) {
    res.boom.badRequest(e)
  }
}
