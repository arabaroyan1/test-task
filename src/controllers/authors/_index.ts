import * as AuthorGet from './authors.get'
import * as AuthorPost from './authors.post'
import * as AuthorPatch from './authors.patch'
import * as AuthorDelete from './authors.delete'

export { AuthorGet, AuthorPost, AuthorPatch, AuthorDelete }
