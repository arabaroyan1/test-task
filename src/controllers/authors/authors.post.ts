import { Request, Response } from 'express'
import { AuthorsDao } from '../../dao/_index'
import {validationResult} from 'express-validator'

export async function create(req: Request, res: Response) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() })
  }

  try {
    const authors: any = await AuthorsDao.create(req.body)
    return  res.status(201).send(authors)
  } catch (e) {
    res.boom.badRequest(e)
  }
}
