import { Request, Response } from 'express'
import { AuthorsDao } from '../../dao/_index'

export async function deleteAuthor(req: Request, res: Response) {
  try {
    if (!req.query.id) {
      return res.status(400).json({
        error: 'Bad Request',
        message: 'missing id'
      })
    }
    const authors: any = await AuthorsDao.deleteAuthor(req.query.id)
    return  res.status(202).send(authors)
  } catch (e) {
    res.boom.badRequest(e)
  }
}
