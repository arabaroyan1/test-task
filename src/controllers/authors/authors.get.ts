import { Request, Response } from 'express'
import { AuthorsDao } from '../../dao/_index'
import {config} from '../../constants'

export async function list(req: Request, res: Response) {
  const limit = config.limit
  const offset = req.query.page ? (req.query.page - 1) * limit : 0
  try {
    const authors: any = await AuthorsDao.findAll(limit, offset, req.query.firstName, req.query.lastName)
    const count: number = await AuthorsDao.getCount(req.query.firstName, req.query.lastName)
    return  res.status(200).send({...authors, ...{count}})
  } catch (e) {
    res.boom.badRequest(e)
  }
}
