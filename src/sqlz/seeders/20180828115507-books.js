
module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('Books', [{
      firstName: 'seeder data1',
      lastName: 'seeder data1',
      authorId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    },
      {
        firstName: 'seeder data2',
        lastName: 'seeder data2',
        authorId: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        firstName: 'seeder data2',
        lastName: 'seeder data2',
        authorId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        firstName: 'seeder data3',
        lastName: 'seeder data3',
        authorId: 5,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        firstName: 'seeder data4',
        lastName: 'seeder data5',
        authorId: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        firstName: 'seeder data6',
        lastName: 'seeder data6',
        authorId: 11,
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        firstName: 'seeder data7',
        lastName: 'seeder data7',
        authorId: 11,
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        firstName: 'seeder data8',
        lastName: 'seeder data8',
        authorId: 8,
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        firstName: 'seeder data9',
        lastName: 'seeder data9',
        authorId: 9,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        firstName: 'seeder data11',
        lastName: 'seeder data11',
        authorId: 21,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        firstName: 'seeder data11',
        lastName: 'seeder data11',
        authorId: 16,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data12',
        lastName: 'seeder data12',
        authorId: 21,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data12',
        lastName: 'seeder data12',
        authorId: 8,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data13',
        lastName: 'seeder data13',
        authorId: 23,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data14',
        lastName: 'seeder data14',
        authorId: 20,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data15',
        lastName: 'seeder data15',
        authorId: 5,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data16',
        lastName: 'seeder data16',
        authorId: 3,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data17',
        lastName: 'seeder data17',
        authorId: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data18',
        lastName: 'seeder data18',
        authorId: 9,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data19',
        lastName: 'seeder data19',
        authorId: 14,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data20',
        lastName: 'seeder data20',
        authorId: 22,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data21',
        lastName: 'seeder data21',
        authorId: 23,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data22',
        lastName: 'seeder data23',
        authorId: 20,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data24',
        lastName: 'seeder data24',
        authorId: 17,
        createdAt: new Date(),
        updatedAt: new Date()
      }]);
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('Books', {
      email: ADMIN_EMAIL
    });
  }
};
