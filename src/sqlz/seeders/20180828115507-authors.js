
module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('Authors', [{
      firstName: 'seeder data1',
      lastName: 'seeder data1',
      createdAt: new Date(),
      updatedAt: new Date()
    },
      {
        firstName: 'seeder data2',
        lastName: 'seeder data2',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        firstName: 'seeder data2',
        lastName: 'seeder data2',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        firstName: 'seeder data3',
        lastName: 'seeder data3',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        firstName: 'seeder data4',
        lastName: 'seeder data5',
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        firstName: 'seeder data6',
        lastName: 'seeder data6',
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        firstName: 'seeder data7',
        lastName: 'seeder data7',
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        firstName: 'seeder data8',
        lastName: 'seeder data8',
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        firstName: 'seeder data9',
        lastName: 'seeder data9',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        firstName: 'seeder data10',
        lastName: 'seeder data10',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        firstName: 'seeder data11',
        lastName: 'seeder data11',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data12',
        lastName: 'seeder data12',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data12',
        lastName: 'seeder data12',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data13',
        lastName: 'seeder data13',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data14',
        lastName: 'seeder data14',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data15',
        lastName: 'seeder data15',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data16',
        lastName: 'seeder data16',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data17',
        lastName: 'seeder data17',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data18',
        lastName: 'seeder data18',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data19',
        lastName: 'seeder data19',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data20',
        lastName: 'seeder data20',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data21',
        lastName: 'seeder data21',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data22',
        lastName: 'seeder data23',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        firstName: 'seeder data24',
        lastName: 'seeder data24',
        createdAt: new Date(),
        updatedAt: new Date()
      }]);
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('Authors', {
      email: ADMIN_EMAIL
    });
  }
};
