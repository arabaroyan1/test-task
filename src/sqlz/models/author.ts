import { Model, STRING, UUID, Deferrable } from 'sequelize'
import sequelize from './_index'
import { Book } from './book'

export class Author extends Model {
  id: number
  firstName: string
  lastName: string
  createdAt: Date
  updatedAt: Date

}

export class AuthorModel {
  id: number
  firstName: string
  lastName: string
  createdAt: Date
  updatedAt: Date
}

Author.init(
  {
    firstName: STRING(255),
    lastName: STRING(255)
  },
  { sequelize, modelName: 'Author' }
)

Book.belongsTo(Author, {
  as: 'authors',
  foreignKey: 'authorId'
})

Author.hasMany(Book, {
  as: 'books',
  foreignKey: 'authorId',
})
