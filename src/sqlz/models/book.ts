import { Model, STRING, NUMBER} from 'sequelize'
import sequelize from './_index'
import { Author } from './author'

export class Book extends Model {
  id: number
  firstName: string
  lastName: string
  authorId:  number
  createdAt: Date
  updatedAt: Date
}

export class BookModel {
  id: number
  firstName: string
  lastName: string
  createdAt: Date
  updatedAt: Date
}

Book.init(
  {
    firstName: STRING(255),
    lastName: STRING(255),
    authorId: NUMBER()
  },
  { sequelize, modelName: 'Book' }
)



